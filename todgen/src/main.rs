#[macro_use]
extern crate tera;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_yaml as serde_fmt;

mod data;
use data::Base;

use serde::Serialize;

use std::io::prelude::*;
use std::io;
use std::fs;

fn main() -> Result<(), ()> {
    let mut tera = compile_templates!("templates/*.tex"); 
    tera.autoescape_on(vec![]);
    let data =
        if let Some(filename) = std::env::args().nth(2) {
            let mut file = fs::File::open(&filename).unwrap();
            let mut s = String::new();
            file.read_to_string(&mut s);
            let user : Base<String> = serde_fmt::from_str(&s).unwrap();
            tera.render("template.tex", &user).unwrap()
        } else {
            let mut file = fs::File::create("defaultconfig.conf").unwrap();
            let user = data::get_me();
            write!(file, "{}", serde_fmt::to_string(&user).unwrap());
            tera.render("template.tex", &user).unwrap()
        };
    if let Some(output) = std::env::args().nth(1) {
        let mut file = fs::File::create(&output).unwrap();
        write!(file, "{}", data);
    }
    Ok(())
}
