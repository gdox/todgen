
#[derive(Serialize, Deserialize)]
pub struct Base<S : AsRef<str>> {
    user : User<S>
}


#[derive(Serialize, Deserialize)]
struct User<S: AsRef<str>> {
    name : S,
    job : S,
    header : Header<S>,
    projects : Vec<Project<S>>,
    skills : Vec<Skill<S>>,
    studies : S,
}

#[derive(Serialize, Deserialize)]
struct Header<S: AsRef<str>> {
    expertise : Vec<S>,
    languages : Vec<S>,
    location : S,
    mobility : Vec<S>,
}

#[derive(Serialize, Deserialize)]
struct Project<S: AsRef<str>> {
    name : S,
    start : S,
    end : S,
    company: S,
    location: S,
    context : S,
    context_short : S,
    achievements : Vec<S>,
    achievements_short : Vec<S>,
    included : bool,
}

#[derive(Serialize, Deserialize)]
struct Skill<S: AsRef<str>> {
    name : S,
    score : usize,
}

pub fn get_me() -> Base<&'static str> {
    Base {
        user : User {
            name : "Gerwin Dox",
            job : "Embedded Software Developer",
            header : Header {
                expertise: vec!["Linux", "C programming", "Rust programming"],
                languages: vec!["Dutch", "English", "French"],
                location : "Lokeren",
                mobility: vec!["East Flanders", "West Flanders"],
            },
            projects : vec![Project {
                name : "Design Project",
                start : "September 2016",
                end : "June 2017",
                company : "UGent",
                location : "Ghent",
                context : "Long context",
                context_short : "Short context",
                achievements : vec!["Learned a thing"],
                achievements_short : vec!["thing"],
                included : true,
            }],
            skills : vec![Skill { name : "C programming", score: 3}, Skill { name : "Rust + Tera", score : 4}],
            studies : "Studied Civil Engineering at the University of Ghent, option Computer Science, 2018"
        }
    }
}
